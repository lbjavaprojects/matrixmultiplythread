import java.util.Arrays;

public class MainClass {
	private static final int ROW_COUNT=80;
	private static final int COL_COUNT=80;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[][] A=new double[ROW_COUNT][COL_COUNT];
		double[][] B=new double[ROW_COUNT][COL_COUNT];
		double[][] C=new double[ROW_COUNT][COL_COUNT];
		for(int i=0;i<ROW_COUNT;i++){
			for(int j=0;j<COL_COUNT;j++){
				A[i][j]=1;
				B[i][j]=1;
				C[i][j]=0;
			}
		}
		System.out.println("Pocz�tkowy stan macierzy:");
		System.out.println("A: "+Arrays.deepToString(A));
		System.out.println("B: "+Arrays.deepToString(B));
		System.out.println("C: "+Arrays.deepToString(C));
		long startTime=System.currentTimeMillis();
		Thread[][] t=new Thread[ROW_COUNT][COL_COUNT];
		for(int i=0;i<ROW_COUNT;i++){
			for(int j=0;j<COL_COUNT;j++){
				t[i][j]=new Thread(new MatrixMultiplier(i, j, A, B, C));
				t[i][j].start();
			}
			
			for(int j=0;j<COL_COUNT;j++){
				try {
					t[i][j].join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		}
		long stopTime=System.currentTimeMillis();
		System.out.println("Ko�cowy stan macierzy po "+(stopTime-startTime)+ ": milisekundach");
		System.out.println("C: "+Arrays.deepToString(C));
	}

}
